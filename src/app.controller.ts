import { Controller, Get, Query } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AppService } from './app.service';
import { ValidateEmailRequestDto } from './dto/api/request/validate-email-request.dto';
import { ValidatePhoneRequestDto } from './dto/api/request/validate-phone-request.dto';
import { GeneralResponse } from './dto/api/response/general-response.dto';
import { ValidateEmailResponseDto } from './dto/api/response/validate-email-response.dto';
import { ValidatePhoneResponseDto } from './dto/api/response/validate-phone-response.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/validatePhone')
  validatePhone(
    @Query() requestDto: ValidatePhoneRequestDto
  ): Observable<GeneralResponse<ValidatePhoneResponseDto>> {
    return this.appService.validatePhone(requestDto);
  }

  @Get('/validateEmail')
  validateEmail(
    @Query() requestDto: ValidateEmailRequestDto
  ): Observable<GeneralResponse<ValidateEmailResponseDto>> {
    return this.appService.validateEmail(requestDto);
  }

  @Get('/healthz')
  healthz(): Observable<GeneralResponse<string>> {
    return this.appService.healthz();
  }
}
