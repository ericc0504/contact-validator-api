import { Injectable } from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { ValidateEmailRequestDto } from './dto/api/request/validate-email-request.dto';
import { ValidatePhoneRequestDto } from './dto/api/request/validate-phone-request.dto';
import { GeneralResponse } from './dto/api/response/general-response.dto';
import { ValidateEmailResponseDto } from './dto/api/response/validate-email-response.dto';
import { ValidatePhoneResponseDto } from './dto/api/response/validate-phone-response.dto';

@Injectable()
export class AppService {
  private client: ClientProxy;

  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: process.env.SERVICE_HOST,
        port: Number(process.env.SERVICE_PORT),
      },
    });
  }

  validatePhone(
    requestDto: ValidatePhoneRequestDto
  ): Observable<GeneralResponse<ValidatePhoneResponseDto>> {
    return this.client.send<GeneralResponse<ValidatePhoneResponseDto>, string>(
      'validatePhone',
      requestDto.phoneNum
    );
  }

  validateEmail(
    requestDto: ValidateEmailRequestDto
  ): Observable<GeneralResponse<ValidateEmailResponseDto>> {
    return this.client.send<GeneralResponse<ValidateEmailResponseDto>, string>(
      'validateEmail',
      requestDto.email
    );
  }

  healthz(): Observable<GeneralResponse<string>> {
    return this.client.send<GeneralResponse<string>>('healthz', '');
  }
}
