import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ValidateEmailRequestDto {
  @ApiProperty({ name: 'email', required: true })
  @IsString()
  @IsNotEmpty()
  email: string;
}
