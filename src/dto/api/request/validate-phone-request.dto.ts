import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ValidatePhoneRequestDto {
  @ApiProperty({ name: 'phoneNum', required: true })
  @IsString()
  @IsNotEmpty()
  phoneNum: string;
}
