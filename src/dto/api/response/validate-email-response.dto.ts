import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class ValidateEmailResponseDto {
  @IsString()
  email: string;

  @IsString()
  did_you_mean: string;

  @IsBoolean()
  format_valid: boolean;

  @IsBoolean()
  mx_found: boolean;

  @IsBoolean()
  smtp_check: boolean;

  @IsBoolean()
  catch_all: boolean;

  @IsBoolean()
  role: boolean;

  @IsBoolean()
  disposable: boolean;

  @IsBoolean()
  free: boolean;

  @IsNumber()
  score: Number;
}
