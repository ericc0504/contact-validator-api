import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

const logger = new Logger('Main');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Swagger
  const config = new DocumentBuilder()
    .setTitle('Contact Validator')
    .setDescription('API for phone number and email validation.')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  
  app.useGlobalPipes(new ValidationPipe());
  if (process.env.ENABLE_CORS === 'true'){
    app.enableCors();
  }
  await app.listen(process.env.PORT, () => {
    logger.log(`App is listening port ${process.env.PORT}...`);
  });
}
bootstrap();
