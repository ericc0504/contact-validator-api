# Contact Validator API
This is a NestJS API for phone number and email validation.

## System Architecture
![System Architecture](ContactValidatorArchitecture.png)

## Prerequisite
This application have to be run together with [contact-validator-frontend](https://bitbucket.org/ericc0504/contact-validator-frontend/src/master/) and [contact-validator-service](https://bitbucket.org/ericc0504/contact-validator-service/src/master/).
Please first check it out.

## Getting Started
```bash
# Clone the repo
$ git clone git@bitbucket.org:ericc0504/contact-validator-api.git

# Install dependencies
$ yarn

# Run
$ docker-compose up
```

## API Documentation
http://localhost:3333/api/#/